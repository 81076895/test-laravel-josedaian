## About
This is a repository that contains the solution to the tasks requested to be hired by Grupo Todo.

## How to install

* clone this repo
* cd test-laravel-josedaian
* composer install
* cp .env.example .env
* edit .env file to configure database credentials
* php artisan key:generate
* php artisan migrate
* php artisan serve


## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
