<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignSubCategoriesRequest;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Repositories\CategoriaRepository;
use App\Traits\AppResponseManager;
use Exception;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Throwable;

class CategoriaController extends Controller
{
    use AppResponseManager;
    /** @var CategoriaRepository */
    protected $categoriaRepository;

    /**
     * @param CategoriaRepository $categoriaRepository 
     * @return void 
     */
    public function __construct(CategoriaRepository $categoriaRepository)
    {
        $this->categoriaRepository = $categoriaRepository;
    }

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function index()
    {
        return view('admin.categoria.index', ['categorias' => $this->categoriaRepository->listAll()]);
    }

    /**
     * @param StoreCategoryRequest $request 
     * @return array|RedirectResponse|void 
     * @throws BindingResolutionException 
     */
    public function store(StoreCategoryRequest $request)
    {
        try {
            $this->categoriaRepository->create($request->only(['nombre', 'descripcion', 'nivel']));
            return $this->successResponse($request);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @param UpdateCategoryRequest $request 
     * @param Categoria $categoria 
     * @return array|RedirectResponse|void 
     * @throws BindingResolutionException 
     */
    public function update(UpdateCategoryRequest $request, Categoria $categoria)
    {
        try {
            $this->categoriaRepository->update($request->only(['nombre', 'descripcion', 'nivel']), $categoria);
            return $this->successResponse($request);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @param Request $request 
     * @param Categoria $categoria 
     * @return array|RedirectResponse|void 
     * @throws BindingResolutionException 
     */
    public function destroy(Request $request, Categoria $categoria)
    {

        try {
            $categoria->productos()->sync([]);
            $categoria->delete();
            return $this->successResponse($request);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function listado()
    {
        return view('front.categoria.index', ['categorias' => $this->categoriaRepository->listAll()]);
    }

    /**
     * @param Request $request 
     * @param Categoria $categoria 
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function detalle(Request $request, Categoria $categoria)
    {
        return view('front.categoria.detail', [
            'categoria' => $categoria, 
            'categorias' => $this->categoriaRepository->listAll() 
        ]);
    }

    /**
     * @param Request $request 
     * @param null|Categoria $categoria 
     * @return array|RedirectResponse 
     * @throws BindingResolutionException 
     */
    public function renderForm(Request $request, ?Categoria $categoria){
        $route = 'categorias.store';
        if($categoria->exists){
            $route = ['categorias.update', $categoria];
        }

        return $this->successResponse($request, [
            'form' => view('admin.categoria.form', [
                'route' => $route,
                'categoria' => $categoria,
                'niveles' => $this->categoriaRepository->getCategoryLevelDropdown(),
            ])->render() 
        ]);
    }

    /**
     * @param Request $request 
     * @param Categoria $categoria 
     * @return array|RedirectResponse 
     * @throws BindingResolutionException 
     * @throws Throwable 
     */
    public function renderSubCategoriasForm(Request $request, Categoria $categoria){
        return $this->successResponse($request, [
            'form' => view('admin.categoria.associate-subcategories-form', [
                'categorias' => $this->categoriaRepository->categoriasSeleccionables($categoria),
                'categoriasSeleccionadas' => $categoria->subcategorias()->pluck('id'),
                'categoria' => $categoria,
            ])->render() 
        ]);
    }

    /**
     * @param AssignSubCategoriesRequest $request 
     * @param Categoria $categoria 
     * @return array|RedirectResponse|void 
     * @throws BindingResolutionException 
     */
    public function asignarSubCategorias(AssignSubCategoriesRequest $request, Categoria $categoria){
        try {
            $this->categoriaRepository->asignarSubCategorias($request->only(['subcategorias']), $categoria);
            return $this->successResponse($request);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }
}
