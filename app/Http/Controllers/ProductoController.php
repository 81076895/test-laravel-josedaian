<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Http\Request;
use App\Models\Producto;
use App\Repositories\CategoriaRepository;
use App\Repositories\ProductoRepository;
use App\Traits\AppResponseManager;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Exception;

class ProductoController extends Controller
{
    use AppResponseManager;

    /** @var ProductoRepository $productoRepository */
    protected $productoRepository;
    /** @var CategoriaRepository $categoriaRepository */
    protected $categoriaRepository;

    /**
     * @param ProductoRepository $productoRepository 
     * @param CategoriaRepository $categoriaRepository 
     * @return void 
     */
    public function __construct(ProductoRepository $productoRepository, CategoriaRepository $categoriaRepository)
    {
        $this->productoRepository = $productoRepository;
        $this->categoriaRepository = $categoriaRepository;
    }

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function index()
    {
        return view('admin.producto.index', [
            'productos' => $this->productoRepository->listAll(), 
            'categorias' => $this->categoriaRepository->listAll()
        ]);
    }

    /**
     * @param Request $request 
     * @param Producto $producto 
     * @return array|RedirectResponse|View|Factory|void 
     */
    public function show(Request $request, Producto $producto)
    {
        try {
            if($request->ajax()){
                return $this->successResponse($request, ['producto' => $producto]);
            }
            return view('admin.producto.detail', ['producto' => $producto]);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @param StoreProductRequest $request 
     * @return array|RedirectResponse|void 
     * @throws BindingResolutionException 
     */
    public function store(StoreProductRequest $request)
    {
        try {

            $this->productoRepository->create($request->only(['nombre', 'descripcion', 'categorias']));
            return $this->successResponse($request);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @param UpdateProductRequest $request 
     * @param Producto $producto 
     * @return array|RedirectResponse 
     * @throws BindingResolutionException 
     */
    public function update(UpdateProductRequest $request, Producto $producto)
    {
        try {
            $this->productoRepository->update($request->only(['nombre', 'descripcion', 'categorias']), $producto);
            return $this->successResponse($request);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @param Request $request 
     * @param Producto $producto 
     * @return array|RedirectResponse|void 
     * @throws BindingResolutionException 
     */
    public function destroy(Request $request, Producto $producto)
    {
        try {
            $producto->categorias()->sync([]);
            $producto->delete();
            return $this->successResponse($request);

        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function homePage()
    {
        return view('front.producto.index', [
            'productos' => $this->productoRepository->listAll(), 
            'categorias' => $this->categoriaRepository->getRootCategories()
        ]);
    }

    /**
     * @param Request $request 
     * @param Producto $producto 
     * @return View|Factory|array|RedirectResponse|void 
     */
    public function detalle(Request $request, Producto $producto)
    {
        try{
            return view('front.producto.detail', ['producto' => $producto]);
        } catch (Exception $exception) {
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse($request);
        }
    }

    /**
     * @param Request $request 
     * @param null|Producto $producto 
     * @return array|RedirectResponse 
     * @throws BindingResolutionException 
     */
    public function renderForm(Request $request, ?Producto $producto){
        $route = 'productos.store';
        if($producto->exists){
            $route = ['productos.update', $producto];
        }

        return $this->successResponse($request, [
            'form' => view('admin.producto.form', [
                'route' => $route,
                'producto' => $producto,
                'categorias' => $this->categoriaRepository->listForDropdown('id', 'nombre')
            ])->render() 
        ]);
    }
}
