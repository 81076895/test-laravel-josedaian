<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomFormRequest extends FormRequest
{
    /**
     * @param \Illuminate\Contracts\Validation\Validator $validator
     */
    protected function failedValidation($validator)
    {
        throw new ValidationException(422, $validator->errors()->first());
    }

    /**
     * Indica si el validador se detendrá en la primera regla que falle.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;
}
