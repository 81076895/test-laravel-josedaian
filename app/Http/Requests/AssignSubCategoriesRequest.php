<?php

namespace App\Http\Requests;

/** @package App\Http\Requests */
class AssignSubCategoriesRequest extends CustomFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subcategorias' => 'array|nullable',
            'subcategorias.*' => 'exists:categorias,id',
        ];
    }

    /** @return array  */
    public function messages()
    {
        return [
            'subcategorias.array' => 'El input recibido debe ser un array',
            'subcategorias.*.exits' => 'Una de las subcategorias seleccionada no es válida',
        ];
    }
}
