<?php

namespace App\Http\Requests;

/** @package App\Http\Requests */
class StoreProductRequest extends CustomFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:50',
            'categorias' => 'required|exists:categorias,id',
            'descripcion' => 'min:5|max:150',
        ];
    }

    /** @return array  */
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es requerido',
            'nombre.min' => 'La longitud minima del nombre debe ser :min carácteres',
            'nombre.max' => 'La longitud máxima del nombre debe ser :max carácteres',
            'categorias.required' => 'La categoria es requerida',
            'categorias.exists' => 'La categoria recibida no es válida',
            'descripcion.min' => 'La longitud minima del descripcion debe ser :min carácteres',
            'descripcion.max' => 'La longitud máxima del descripcion debe ser :max carácteres',
        ];
    }
}
