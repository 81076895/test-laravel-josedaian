<?php

namespace App\Http\Requests;

/** @package App\Http\Requests */
class UpdateCategoryRequest extends CustomFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'   => 'required|min:4|max:50',
            'descripcion'   => 'nullable|min:4|max:150',
            'nivel'   => 'required',
        ];
    }

    /** @return array  */
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es requerido',
            'nombre.min' => 'La longitud minima del nombre debe ser :min carácteres',
            'nombre.max' => 'La longitud máxima del nombre debe ser :max carácteres',
            'descripcion.min' => 'La longitud minima del descripcion debe ser :min carácteres',
            'descripcion.max' => 'La longitud máxima del descripcion debe ser :max carácteres',
            'nivel.required' => 'El nivel es requerido',
        ];
    }
}
