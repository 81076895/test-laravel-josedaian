<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductoCategoria extends Pivot
{
    protected $table = 'producto_categorias';
    /** @var string[] */
    protected $fillable = ['producto_id', 'categoria_id'];

    /** @return HasOne  */
    public function producto(){
        return $this->hasOne(Producto::class);
    }

    /** @return HasOne  */
    public function categoria(){
        return $this->hasOne(Categoria::class);
    }
}
