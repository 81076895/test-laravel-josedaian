<?php

namespace App\Models;

use App\Traits\BadgeElement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Categoria extends Model
{   
    use BadgeElement;

    CONST ROOT_LEVEL = 1;

    /** @var string[] */
    protected $fillable = ['nombre', 'descripcion', 'nivel'];

    /** @return BelongsToMany  */
    public function productos(){
        return $this->belongsToMany(Producto::class, 'producto_categorias')->using(ProductoCategoria::class);
    }

    /** @return HasMany  */
    public function subcategorias(){
        return $this->hasMany(Categoria::class, 'parent_id');
    }

    /** @return BelongsTo  */
    public function categoriaPadre(){
        return $this->belongsTo(Categoria::class, 'parent_id');
    }

    /** @return string  */
    public function getSubcategoriasBadgesAttribute(){
        $badges = '';
        foreach($this->subcategorias as $subcategoria){
            $badges .= $this->renderBadge('primary', $subcategoria->nombre);
        }
        return $badges;
    }

}
