<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Producto extends Model
{
    /** @var string[] */
    protected $fillable = ['nombre', 'descripcion'];

    /** @return BelongsToMany  */
    public function categorias(){
        return $this->belongsToMany(Categoria::class, 'producto_categorias')->using(ProductoCategoria::class);
    }
}
