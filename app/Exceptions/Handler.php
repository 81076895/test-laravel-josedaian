<?php

namespace App\Exceptions;

use BadMethodCallException;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return redirect()->route('login')->withInput($request->except('pass'))->with(['error' => 'Tiempo de inactividad expirado, intente de nuevo.']);
        }

        if ($exception instanceof ValidationException) {
            return redirect()->back()->withInput()->with('error', $exception->getResponse());
        }

        
        $messsage = $exception->getMessage();

        if ($exception instanceof HttpException) {
            $messsage = $exception->getMessage();
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            $messsage = __('El método especificado para la solicitud no es válido.');
        }
        
        if ($exception instanceof NotFoundHttpException) {
            $messsage = __('Recurso no existe (URL)');
        }
        
        if ($exception instanceof ModelNotFoundException) {
            $messsage = __('El producto no es válido o no existe');
        }
        
        if ($exception instanceof QueryException) {
            $messsage = __('Error de base de datos');
        }

        if ($exception instanceof BadMethodCallException){
            $messsage = __('El método no existe o no se encuentra habilitado');
        }


        return  response(view('errors.exception', ['message' => $messsage]));
    }
}
