<?php
namespace App\Repositories;

use App\Models\Categoria;

class CategoriaRepository extends BaseRepository {

    /**
     * @param Categoria $categoria 
     * @return void 
     */   
    public function __construct(Categoria $categoria)
    {
        parent::__construct($categoria);
    }
    
    /**
     * @param Categoria $categoria 
     * @return mixed 
     */
    public function categoriasSeleccionables(Categoria $categoria){
        $denyIds = [];
        $denyIds[] = $categoria->id;
        if($categoria->parent_id){
            $denyIds[] = $categoria->parent_id;
        }

        return $this->model::where(function($query) use($categoria, $denyIds){
            $query->orWhere('parent_id', $categoria->id);
            $query->orWhere(function($sql) use($denyIds){
                $sql->whereNotIn('id', $denyIds)->whereNull('parent_id');
                
                if($this->countRootCategories() <= 1){
                    $sql->where('nivel', '<>', Categoria::ROOT_LEVEL);
                }

            });
        })->where('nivel', '>=', $categoria->nivel)->pluck('nombre', 'id');
    }

    /**
     * @param array $attributes 
     * @param Categoria $categoria 
     * @return mixed 
     */
    public function asignarSubCategorias(array $attributes, Categoria $categoria){
        $this->model::where(['parent_id' => $categoria->id])->update(['parent_id' => null]);
        if(!empty($attributes)){
            return $this->model::whereIn('id', $attributes['subcategorias'])->update(['parent_id' => $categoria->id]);
        }
    }

    /** @return array  */
    public function getCategoryLevelDropdown(){
        return array_combine(range(1, $this->getLastCategoryLevel()), range(Categoria::ROOT_LEVEL, $this->getLastCategoryLevel()));
    }

    /** @return mixed  */
    public function getLastCategoryLevel(){
        return $this->model::max('nivel') + 1;
    }

    /**
     * Retorna todas las categorias que se encuentren en el nivel 1 y que no tengan categorias padres
     * @return mixed  
     * */
    public function getRootCategories(){
        return $this->model::where('nivel', Categoria::ROOT_LEVEL)->whereNull('parent_id')->get();
    }

    /** @return mixed  */
    public function countRootCategories(){
        return $this->model::where('nivel', Categoria::ROOT_LEVEL)->whereNull('parent_id')->count();
    }
}