<?php
namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;

class BaseRepository 
{     
    /**      
     * @var Model      
     */     
     protected $model;       

    /**      
     * BaseRepository constructor.      
     *      
     * @param Model $model      
     */     
    public function __construct(Model $model)     
    {         
        $this->model = $model;
    }
 
    /**
    * @param array $attributes
    *
    * @return Model
    */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }
 
    /**
    * @param $id
    * @return Model
    */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * @param mixed|null $relations 
     * @return Builder|Model 
     */
    public function datatableQuery($relations = null){
        return $relations ? $this->model::with($relations) : $this->model;
    }

    /**
     * @param array $attributes 
     * @return Model 
     * @throws MassAssignmentException 
     */
    public function update(array $attributes, Model $model){

        $model->fill($attributes)->save();
        return $model;
    }

    /**
     * @param mixed|null $relations 
     * @return Collection<mixed, (Builder|Model)>|Collection<mixed, Model> 
     */
    public function listAll($relations = null){
        return $relations ? $this->model::with($relations)->all() : $this->model->all();
    }

    /**
     * @param mixed $key 
     * @param mixed $value 
     * @return mixed 
     */
    public function listForDropdown($key, $value){
        return $this->model->pluck($value, $key);
    }

    /** @return Model  */
    public function getModel(): Model {
        return $this->model;
    }

}