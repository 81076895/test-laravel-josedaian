<?php
namespace App\Repositories;

use App\Models\Producto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\MassAssignmentException;

class ProductoRepository extends BaseRepository {

    /**
     * @param Producto $producto 
     * @return void 
     */   
    public function __construct(Producto $producto)
    {
        parent::__construct($producto);
    }

    /**
     * @param array $attributes 
     * @return Producto 
     */
    public function create(array $attributes): Producto{
        $categories = @$attributes['categorias'];
        unset($attributes['categorias']);

        $record = parent::create($attributes);

        if(!empty($categories)){
            $record->categorias()->sync($categories);
        }

        return $record;
    }

    /**
     * @param array $attributes 
     * @return Model 
     * @throws MassAssignmentException 
     */
    public function update(array $attributes, Model $producto){
        $categories = @$attributes['categorias'];
        unset($attributes['categorias']);

        $record = parent::update($attributes, $producto);

        if(!empty($categories)){
            $record->categorias()->sync($categories);
        }

        return $record;
    }
}