<?php
namespace App\Traits;

use Illuminate\Contracts\Container\BindingResolutionException;

trait BadgeElement {
    /**
     * @param string $type 
     * @param string $text 
     * @return string 
     */
    public function renderBadge(string $type, string $text){
        return "<span class='m-1 badge badge-{$type}'>{$text}</span>";
    }
}