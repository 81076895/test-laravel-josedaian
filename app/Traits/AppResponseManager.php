<?php
namespace App\Traits;

use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

trait AppResponseManager {

    /**
     * @param Request $request 
     * @param mixed|null $data 
     * @param string $message 
     * @param string $type 
     * @return array|RedirectResponse 
     * @throws BindingResolutionException 
     */
    public function successResponse(Request $request, $data = null, string $message = 'La operación ha sido registrada', $type = 'success'){
        if($request->ajax()){
            return [
                'error' => false,
                'message' => __($message),
                'type' => $type,
                'title' => 'Éxito!',
                'data' => $data
            ];
        }
        
        return redirect()->back()->with($type, $message);
    }

    /**
     * @param Request $request 
     * @param string $route 
     * @param mixed|null $data 
     * @param string $message 
     * @param string $type 
     * @return array|RedirectResponse 
     * @throws BindingResolutionException 
     * @throws RouteNotFoundException 
     */
    public function successResponseAndRedirect(Request $request, string $route, $data = null, string $message = 'La operación ha sido registrada', $type = 'success'){
        if($request->ajax()){
            $this->errorResponse($request, $message = 'La operación fue registrada, pero se esperaba una redirección.', 'error', 'bad_request.ajax');
        }

        return redirect()->route($route)->with($type, $message);
    }

    /**
     * @param Request $request
     * @param string $message
     * @param string $type
     * @param string|null $code Codigo de información que nos ayudara a identificar mas rápidamente el error
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function errorResponse(Request $request, string $message = 'Ha ocurrido un error interno en el servidor.', string $type = 'error', ?string $code = null){
        if($request->ajax()){
            return [
                'error' => true,
                'message' => __($message),
                'type' => $type,
                'title' => 'Atención!',
                'code' => $code
            ];
        }

        return redirect()->back()->with($type, $message)->withInput();
    }
}
