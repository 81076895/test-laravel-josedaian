<?php
namespace App\Traits;

use Illuminate\Contracts\Container\BindingResolutionException;

trait CommonDtButtonActions {
    /**
     * @param mixed $recordId 
     * @return string 
     * @throws BindingResolutionException 
     */
    public function renderButtonActions($recordId){
        $updateButton = $this->renderButton('updateRecord', __('Actualizar'), 'far fa-edit', $recordId);
        $deleteButton = $this->renderButton('deleteRecord', __('Eliminar'), 'far fa-trash-alt', $recordId);
        $showButton = $this->renderButton('showRecord', __('Ver'), 'far fa-eye', $recordId);

        return $showButton . $updateButton . $deleteButton;
    }

    /**
     * @param string $classNames 
     * @param string $title 
     * @param mixed $recordId 
     * @return string 
     */
    public function renderButton(string $classNames, string $title, string $icon, $recordId){
        return "<a href='javascript:void(0)' class='m-1 {$classNames}' title='{$title}' data-popup='tooltip' data-placement='top' record-id='{$recordId}'><i class='{$icon}'></i></a>";
    }
}