<!-- Basic modal -->
<div class="modal-header bg-blue-800">
    <h5 class="modal-title">
        <i class="icon-plus22"></i>
        {{ __('Categoria #'.$categoria->id ) }}
    </h5>
    <button type="button" class="close" data-dismiss="modal">×</button>
</div>

{!! Form::open(['route' =>  ['categorias.asignar_subcategorias', $categoria->id], 'method' => 'POST', 'role' => 'form', 'id' => 'categoria-form']) !!}
<div class="modal-body">
    <div class="form-group">
        {!! Form::label('subcategorias', __('Descripción')) !!}
        {!! Form::select('subcategorias[]', $categorias, $categoriasSeleccionadas, ['class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        <small class="text-muted"><strong>Nota:</strong> no son seleccionables: <br>
            <ul>
                <li>la <strong>propia categoria</strong></li>
                <li>la <strong>categoria padre</strong></li>
                <li>categorias de <strong>niveles superiores</strong></li>
            </ul>
        </small>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-link" data-dismiss="modal">{{ __('Cancelar') }}</button>
    <button type="submit" class="btn bg-primary">{{ __('Guardar') }}</button>
</div>

{!! Form::close() !!}
<!-- /basic modal -->