@extends('admin.layouts.default')

@section('page-title', __('ABM Categorias'))

@section('content')

    @include('partials.breadcrumbs', ['breadcrumbs' => [
        'Admin',
        'Categorias'
    ]])
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Categorias') }}
                        <button type="button" class="btn btn-sm btn-success btn-icon render-form"><i class="icon-plus22"></i>
                            {{ __('nuevo registro') }}</button>
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Nombre') }}</th>
                                        <th>{{ __('Nivel en la Jerarquía') }}</th>
                                        <th>{{ __('Categoria Padre') }}</th>
                                        <th>{{ __('Subcategorias') }}</th>
                                        <th>{{ __('Acciones') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($categorias->isEmpty())
                                        <tr>
                                            <td colspan="6" class="text-center">{{ __('No hay registros para mostrar') }}</td>
                                        </tr>
                                    @endif

                                    @foreach ($categorias as $categoria)
                                        <tr>
                                            <td>{{ $categoria->id }}</td>
                                            <td>{{ $categoria->nombre }}</td>
                                            <td>{{ $categoria->nivel }}</td>
                                            <td>{{ $categoria->categoriaPadre ? $categoria->categoriaPadre->nombre : __('(no definido)') }}</td>
                                            <td> {!! $categoria->subcategorias_badges !!} </td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-sm asignar-subcategorias" record-id="{{ $categoria->id }}">{{ __('Asignar Subcategorias') }}</button>
                                                <button type="button" class="btn btn-info btn-sm update-record" record-id="{{ $categoria->id }}">{{ __('Editar') }}</button>
                                                <button type="button" class="btn btn-danger btn-sm delete-record" record-id="{{ $categoria->id }}">{{ __('Eliminar') }}</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('admin.layouts._common_modal_form')
@endsection

@section('css')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        let renderFormUrl = '{{ route('categorias.render_form') }}';
        let renderSubCategoriasFormUrl = '{{ route('categorias.render_subcategorias_form', ['categoria' => ':id']) }}';
        let deleteRecordUrl = '{{ route('categorias.destroy', ['categoria' => ':id']) }}';

        $(document).on('click', '.render-form,.update-record', function() {
            var categoriaId = $(this).attr('record-id') ?? '';
            $.get(renderFormUrl + '/' + categoriaId).done(function(response) {
                $('#modalForm .modal-content').html(response.data.form);
                $('#modalForm').modal('show');
                formValidate();
            });
        });

        $(document).on('click', '.asignar-subcategorias', function() {
            var categoriaId = $(this).attr('record-id') ?? '';
            console.log(categoriaId);
            console.log(renderSubCategoriasFormUrl);
            $.get(renderSubCategoriasFormUrl.replace(':id', categoriaId)).done(function(response) {
                $('#modalForm .modal-content').html(response.data.form);
                $('#modalForm').modal('show');
                $('.select2').select2();
            });
        });

        function formValidate() {
            $('#categoria-form').validate({
                rules: {
                    'nombre': {
                        required: true
                    },
                    'nivel': {
                        required: true
                    }
                },
                messages: {
                    'nombre': {
                        required: "Ingrese el nombre"
                    },
                    'nivel': {
                        required: "Debe seleccionar una opción"
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
            });
        }

        $(document).on('click', '.delete-record', function(){
            var categoriaId = $(this).attr('record-id') ?? '';
            if(!confirm('Esta seguro que desea continuar con la operación?') || typeof categoriaId === 'undefined')
                return false;
                $.ajax({
                    url: deleteRecordUrl.replace(':id', categoriaId),
                    type: 'DELETE',
                    data: {_token: '{{ csrf_token() }}'}
                }).done(function(response){
                    if(response.error){
                        return alert(response.message);
                    }
                    location.reload();
                });

        });
    </script>
@endsection
