<!-- Basic modal -->
<div class="modal-header bg-blue-800">
    <h5 class="modal-title">
        <i class="icon-plus22"></i>
        {{ $categoria->exists ? __('Modificar Categoria #'.$categoria->id ) : __('Nueva Categoria') }}
    </h5>
    <button type="button" class="close" data-dismiss="modal">×</button>
</div>

{!! Form::model($categoria, ['route' => $route , 'method' => 'POST', 'role' => 'form', 'id' => 'categoria-form']) !!}
@if ($categoria->exists)
    {!! Form::hidden('_method', 'PUT') !!}
@endif

<div class="modal-body">
    
    <div class="form-group">
        {!! Form::label('nombre', __('Nombre')) !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => __('Ingrese el nombre')]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('descripcion', __('Descripción')) !!}
        {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => __('Ingrese la descripción')]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('nivel', __('Nivel dentro de la jerarquia')) !!}
        {!! Form::select('nivel', $niveles, null, ['class' => 'form-control select2', 'placeholder' => 'Seleccione una opción', 'disabled' => $categoria->exists]) !!}
        <small>La jerarquia se tomara en cuenta de menor a mayor. Es decir, el nivel 1 es el nivel mas alto</small>
    </div>

    @if ($categoria->exists)
    <div class="form-check">
        {!! Form::checkbox('regenerar_niveles', true, null, ['id' => 'regenerar_niveles', 'class' => 'form-check-input']) !!}
        <label for="regenerar_niveles" class="form-check-label">Desea modificar el nivel de la categoria? Este cambio afectara a todas las subcategorias que dependen de esta</label>
    </div>
    @endif
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-link" data-dismiss="modal">{{ __('Cancelar') }}</button>
    <button type="submit" class="btn bg-primary">{{ __('Guardar') }}</button>
</div>

{!! Form::close() !!}
<!-- /basic modal -->