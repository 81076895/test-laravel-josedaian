@if (Session::has('success'))
    <div class="row p-3">
        <div class="col col-md-12">
            <div class="alert alert-success alert-styled-left alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span class="font-weight-semibold"> Éxito! </span> {{ Session::get('success') }}
            </div>
        </div>
    </div>
@endif
@if (Session::has('error'))
    <div class="row p-3">
        <div class="col col-md-12">
            <div class="alert alert-danger alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                <span class="font-weight-semibold">Atención!</span> {{ Session::get('error') }}
            </div>
        </div>
    </div>
@endif
