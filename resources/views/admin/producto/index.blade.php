@extends('admin.layouts.default')

@section('page-title', __('ABM Productos'))

@section('content')
    @include('partials.breadcrumbs', ['breadcrumbs' => [
        'Admin',
        'Productos'
    ]])
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Productos') }}
                        <button type="button" class="btn btn-sm btn-success btn-icon render-form"><i class="icon-plus22"></i>
                            {{ __('nuevo registro') }}</button>
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Nombre') }}</th>
                                        <th>{{ __('Descripción') }}</th>
                                        <th>{{ __('Categoria') }}</th>
                                        <th>{{ __('Acciones') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($productos->isEmpty())
                                        <tr>
                                            <td colspan="5" class="text-center">{{ __('No hay registros para mostrar') }}</td>
                                        </tr>
                                    @endif
                                    @foreach ($productos as $producto)
                                        <tr>
                                            <td>{{ $producto->id }}</td>
                                            <td>
                                                {!! link_to_route('productos.show', $producto->nombre, ['producto' => $producto->id], []) !!}
                                            </td>
                                            <td>{{ $producto->descripcion }}</td>
                                            <td>{{ $producto->categorias->first() ? $producto->categorias->first()->nombre : __('(no definido)') }}</td>
                                            <td>
                                                <button type="button" class="btn btn-info btn-sm update-record" record-id="{{ $producto->id }}">{{ __('Editar') }}</button>
                                                <button type="button" class="btn btn-danger btn-sm delete-record" record-id="{{ $producto->id }}">{{ __('Eliminar') }}</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection  


@section('modals')
    @include('admin.layouts._common_modal_form')
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script>
        let renderFormUrl = '{{ route('productos.render_form') }}';
        let deleteRecordUrl = '{{ route('productos.destroy', ['producto' => ':id']) }}';

        $(document).on('click', '.render-form,.update-record', function() {
            var productoId = $(this).attr('record-id') ?? '';
            $.get(renderFormUrl + '/' + productoId).done(function(response) {
                $('#modalForm .modal-content').html(response.data.form);
                $('#modalForm').modal('show');
                formValidate();
            });
        });

        function formValidate() {
            $('#producto-form').validate({
                rules: {
                    'nombre': {
                        required: true
                    },
                    'descripcion': {
                        required: true
                    },
                    'categorias[]': {
                        required: true
                    }
                },
                messages: {
                    'nombre': {
                        required: "Ingrese el nombre"
                    },
                    'descripcion': {
                        required: "Ingrese la descripción"
                    },
                    'categorias[]': {
                        required: "Seleccione una categoria"
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
            });
        }

        $(document).on('click', '.delete-record', function(){
            var productoId = $(this).attr('record-id') ?? '';
            if(!confirm('Esta seguro que desea continuar con la operación?') || typeof productoId === 'undefined')
                return false;
                $.ajax({
                    url: deleteRecordUrl.replace(':id', productoId),
                    type: 'DELETE',
                    data: {_token: '{{ csrf_token() }}'}
                }).done(function(response){
                    if(response.error){
                        return alert(response.message);
                    }
                    location.reload();
                });

        });
    </script>
@endsection