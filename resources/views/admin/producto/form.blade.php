<!-- Basic modal -->
<div class="modal-header bg-blue-800">
    <h5 class="modal-title">
        <i class="icon-plus22"></i>
        {{ $producto->exists ? __('Modificar Producto #'.$producto->id ) : __('Nuevo Producto') }}
    </h5>
    <button type="button" class="close" data-dismiss="modal">×</button>
</div>

{!! Form::model($producto, ['route' => $route , 'method' => 'POST', 'role' => 'form', 'id' => 'producto-form']) !!}
@if ($producto->exists)
    {!! Form::hidden('_method', 'PUT') !!}
@endif

<div class="modal-body">
    
    <div class="form-group">
        {!! Form::label('nombre', __('Nombre')) !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => __('Ingrese el nombre')]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('descripcion', __('Descripción')) !!}
        {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => __('Ingrese la descripción')]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('categorias', __('Categoria')) !!}
        {!! Form::select('categorias[]', $categorias, null, ['class' => 'form-control select2', 'placeholder' => 'Seleccione una opción']) !!}
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-link" data-dismiss="modal">{{ __('Cancelar') }}</button>
    <button type="submit" class="btn bg-primary">{{ __('Guardar') }}</button>
</div>

{!! Form::close() !!}
<!-- /basic modal -->