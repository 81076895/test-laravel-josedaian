@extends('admin.layouts.default')

@section('page-title', $producto->nombre)

@section('content')
    @include('partials.breadcrumbs', ['breadcrumbs' => [
        'Admin',
        'Productos' => route('productos.index'),
        $producto->nombre
    ]])

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Producto #'.$producto->id.': ').$producto->nombre }}
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <ul class="list-group">
                                <li class="list-group-item"><strong>{{ __('Nombre:') }}</strong> {{ $producto->nombre }}</li>
                                <li class="list-group-item"><strong>{{ __('Descripción:') }}</strong> {{ $producto->descripcion }}</li>
                                <li class="list-group-item">
                                    <strong>
                                        {{ __('Categorias asociadas') }}
                                    </strong>
                                    <ul class="list-group">
                                        @foreach( $producto->categorias as $categoria )
                                            <li class="list-group-item">
                                                {!! link_to_route('front.categorias.detalle', $categoria->nombre, ['categoria' => $categoria->id],[]) !!}
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
