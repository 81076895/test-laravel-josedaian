@extends('front.layouts.default')

@section('page-title', __('Productos'))

@section('content')
    @include('partials.breadcrumbs', ['breadcrumbs' => [
        'Home' => route('productos.home_page'),
        'Productos',
    ]])

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Jerarquia de Categorias') }}
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <ul>
                                @each('front.categoria.partials.tree', $categorias, 'categoria')
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-1">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Productos') }}
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Nombre') }}</th>
                                        <th>{{ __('Descripción') }}</th>
                                        <th>{{ __('Categoria') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($productos->isEmpty())
                                        <tr>
                                            <td colspan="4" class="text-center">{{ __('No hay registros para mostrar') }}</td>
                                        </tr>
                                    @endif
                                    @foreach ($productos as $producto)
                                        <tr>
                                            <td>{{ $producto->id }}</td>
                                            <td>
                                                {!! link_to_route('front.productos.detalle', $producto->nombre, ['producto' => $producto->id],[]) !!}
                                            </td>
                                            <td>{{ $producto->descripcion }}</td>

                                            <td>{!! $producto->categorias->first() ? link_to_route('front.categorias.detalle', $producto->categorias->first()->nombre, ['categoria' => $producto->categorias->first()->id],[])  : __('(no definido)') !!}</td>
                                            
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
