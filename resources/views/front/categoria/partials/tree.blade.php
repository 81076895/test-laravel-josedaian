<li>
    {!! link_to_route('front.categorias.detalle', $categoria->nombre, ['categoria' => $categoria->id], []) !!}
    <ul>
        @each('front.categoria.partials.tree', $categoria->subcategorias, 'categoria')
    </ul>
</li>