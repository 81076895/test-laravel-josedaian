@extends('front.layouts.default')

@section('page-title', __('Productos'))

@section('content')

    @include('partials.breadcrumbs', ['breadcrumbs' => [
        'Home' => route('productos.home_page'),
        'Categorias' => route('front.categorias.listado'),
        $categoria->nombre
    ]])

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Jerarquia de la Categoria: ').$categoria->nombre }}
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <ul>
                                @each('front.categoria.partials.tree', $categoria->subcategorias, 'categoria')
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Categoria #'.$categoria->id.': ').$categoria->nombre }}
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <ul class="list-group">
                                <li class="list-group-item"><strong>{{ __('Nombre:') }}</strong> {{ $categoria->nombre }}</li>
                                <li class="list-group-item"><strong>{{ __('Descripción:') }}</strong> {{ $categoria->descripcion }}</li>
                                <li class="list-group-item">
                                    <strong>
                                        Productos asociados
                                    </strong>
                                    <ul class="list-group">
                                         @foreach( $categoria->productos as $producto )
                                            <li class="list-group-item">
                                                {!! link_to_route('front.productos.detalle', $producto->nombre, ['producto' => $producto->id],[]) !!}
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

