@extends('front.layouts.default')

@section('page-title', __('Categorias'))

@section('content')

    @include('partials.breadcrumbs', ['breadcrumbs' => [
        'Home' => route('productos.home_page'),
        'Categorias'
    ]])
    <div class="row mt-1">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ __('Categorias') }}
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Nombre') }}</th>
                                        <th>{{ __('Descripción') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($categorias->isEmpty())
                                        <tr>
                                            <td colspan="4" class="text-center">{{ __('No hay registros para mostrar') }}</td>
                                        </tr>
                                    @endif
                                    @foreach ($categorias as $categoria)
                                        <tr>
                                            <td>{{ $categoria->id }}</td>
                                            <td>
                                                {!! link_to_route('front.categorias.detalle', $categoria->nombre, ['categoria' => $categoria->id],[]) !!}
                                            </td>
                                            <td>{{ $categoria->descripcion }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
