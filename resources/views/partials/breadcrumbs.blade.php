@if (!empty($breadcrumbs))
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $label => $link)
            <li class="breadcrumb-item">
                @if (is_int($label) && !is_int($link))
                    <a >
                        <span>{{ $link }}</span>
                    </a>
                @else
                    <a href="{{ $link }}">
                        <span class="breadcrumbs-link-text">{{ $label }}</span>
                    </a>
                @endif
            </li>
        @endforeach
    </ol>
</nav>
@endif
