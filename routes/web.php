<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ProductoController;
use App\Http\Controllers\CategoriaController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', [ProductoController::class, 'homePage'])->name('productos.home_page');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){
    #producto
    Route::get('/productos/render-form/{producto?}', [ProductoController::class, 'renderForm'])->name('productos.render_form');
    Route::resource('productos', 'ProductoController');
    
    #categoria
    Route::get('/categorias/render-form/{categoria?}', [CategoriaController::class, 'renderForm'])->name('categorias.render_form');
    Route::get('/categorias/{categoria}/render-subcategorias-form/', [CategoriaController::class, 'renderSubCategoriasForm'])->name('categorias.render_subcategorias_form');
    Route::post('/categorias/{categoria}/asignar-subcategorias', [CategoriaController::class, 'asignarSubCategorias'])->name('categorias.asignar_subcategorias');
    Route::resource('categorias', 'CategoriaController');
});

#Front
Route::group(['prefix' => 'front'], function () {

    #producto
    Route::prefix('productos')->group(function () {
        Route::get('/', [ProductoController::class, 'homePage'])->name('front.productos.listado');
        Route::get('/{producto}', [ProductoController::class, 'detalle'])->name('front.productos.detalle');
    });

    #categoria
    Route::prefix('categorias')->group(function () {
        Route::get('/', [CategoriaController::class, 'listado'])->name('front.categorias.listado');
        Route::get('/{categoria}', [CategoriaController::class, 'detalle'])->name('front.categorias.detalle');
    });
});

Auth::routes();
